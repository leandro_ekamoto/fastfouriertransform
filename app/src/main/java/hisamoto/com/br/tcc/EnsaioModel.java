package hisamoto.com.br.tcc;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hisamoto on 04/06/17.
 */

public class EnsaioModel {

    private DBHelper dbHelper;
    private Context context;

    public EnsaioModel(Context context) {

        this.dbHelper = new DBHelper(context);
        this.context = context;
    }

    public long inserirEnsaio(ContentValues values) {

        // Validação de campos e regra de negócio
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.beginTransaction();

        long id = -1;

        try {

            id = db.insert("ensaio", null, values);
            db.setTransactionSuccessful();
        } catch (android.database.SQLException e) {

            e.printStackTrace();
        } finally {

            db.endTransaction();
        }

        db.close();

        return id != -1 ? id : 0;
    }

    public long inserirEnsaioPontos(ContentValues values) {

        // Validação de campos e regra de negócio
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.beginTransaction();

        long id = -1;

        try {

            id = db.insert("ensaio_pontos", null, values);
            db.setTransactionSuccessful();
        } catch (android.database.SQLException e) {

            e.printStackTrace();
        } finally {

            db.endTransaction();
        }

        db.close();

        return id != -1 ? id : 0;
    }

    public List<Ensaio> getEnsaios() {

        // Validação de campos e regra de negócio
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        db.beginTransaction();

        List<Ensaio> lista = new ArrayList<Ensaio>();

        try {

            final Cursor c = db.query("ensaio", new String[]{"_id, titulo, largura_seccao, altura_seccao, massa_viga, vao_peca, me, frequencia"}, null, null, null, null, null);

            if (c != null) {

                c.moveToFirst();

                while (c.isAfterLast() == false) {

                    lista.add(new Ensaio(c.getString(1), c.getInt(0), c.getDouble(2), c.getDouble(3), c.getDouble(4), c.getDouble(5), c.getDouble(6), c.getDouble(6)));
                    c.moveToNext();
                }
            }


            db.setTransactionSuccessful();
        } catch (android.database.SQLException e) {

            e.printStackTrace();
        } finally {

            db.endTransaction();
        }

        db.close();
        return lista;
    }

    public Ensaio getEnsaio(int id_ensaio) {

        // Validação de campos e regra de negócio
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        db.beginTransaction();

        try {

            final Cursor c = db.query("ensaio", new String[]{"_id, titulo, largura_seccao, altura_seccao, massa_viga, vao_peca, me, frequencia"}, "_id = "+id_ensaio, null, null, null, null);

            if (c != null) {

                c.moveToFirst();
                return new Ensaio(c.getString(1), c.getInt(0), c.getDouble(2), c.getDouble(3), c.getDouble(4), c.getDouble(5), c.getDouble(6), c.getDouble(7));
            }

            db.setTransactionSuccessful();
        } catch (android.database.SQLException e) {

            e.printStackTrace();
        } finally {

            db.endTransaction();
        }

        db.close();
        return null;
    }

    public List<EnsaioPonto> getEnsaioPontos() {

        // Validação de campos e regra de negócio
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        db.beginTransaction();

        List<EnsaioPonto> lista = new ArrayList<EnsaioPonto>();

        try {

            final Cursor c = db.query("ensaio_pontos", new String[]{"_id, _id_ensaio", "ponto", "pontoX"}, null, null, null, null, null);

            if (c != null) {

                c.moveToFirst();

                while (c.isAfterLast() == false) {

                    lista.add(new EnsaioPonto(c.getLong(0), c.getLong(1), c.getDouble(2), c.getDouble(3)));
                    c.moveToNext();
                }
            }


            db.setTransactionSuccessful();
        } catch (android.database.SQLException e) {

            e.printStackTrace();
        } finally {

            db.endTransaction();
        }

        db.close();
        return lista;
    }

    public List<EnsaioPonto> getEnsaioIDPontos(int ensaio_id) {

        // Validação de campos e regra de negócio
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        db.beginTransaction();

        List<EnsaioPonto> lista = new ArrayList<EnsaioPonto>();

        try {

            final Cursor c = db.query("ensaio_pontos", new String[]{"_id, _id_ensaio", "ponto", "pontoX"}, "_id_ensaio="+ensaio_id, null, null, null, null);

            if (c != null) {

                c.moveToFirst();

                while (c.isAfterLast() == false) {

                    lista.add(new EnsaioPonto(c.getLong(0), c.getLong(1), c.getDouble(2), c.getDouble(3)));
                    c.moveToNext();
                }
            }


            db.setTransactionSuccessful();
        } catch (android.database.SQLException e) {

            e.printStackTrace();
        } finally {

            db.endTransaction();
        }

        db.close();
        return lista;
    }
}
