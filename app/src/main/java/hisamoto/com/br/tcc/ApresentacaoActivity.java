package hisamoto.com.br.tcc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ApresentacaoActivity extends AppCompatActivity {

    private TextView descricaoAplicacao;
    private Button proximo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.tela_inicial);

        descricaoAplicacao = (TextView)findViewById(R.id.descricao_aplicacao);
        proximo = (Button)findViewById(R.id.proximo);

        String formattedText = "" +
                "<h1>MOE-Wood<h1>" +
                "<p style=\\\"text-align=\\\"justify\\\" \\\">Aplicativo criado para calcular o Módulo de Elasticidade(MOE) utilizando o método de Vibração Transversal</p>";
        descricaoAplicacao.setText(Html.fromHtml(formattedText));

        proximo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(ApresentacaoActivity.this, DemonstracaoActivity.class);
                startActivity(it);
            }
        });
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

    }
}
