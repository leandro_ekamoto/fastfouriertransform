package hisamoto.com.br.tcc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class DemonstracaoActivity extends AppCompatActivity {

    private Button realizarEnsaio;
    private TextView descricaoDemonstracao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demonstracao);

        realizarEnsaio = (Button)findViewById(R.id.realizar_ensaio);
        descricaoDemonstracao = (TextView)findViewById(R.id.descricao_demonstracao);
        String formattedText = "" +
                "<h1>Demonstração<h1>" +
                "<p style=\\\"text-align=\\\"justify\\\" \\\">Para realizar ensaios informe as configurações da peça de madeira, posicione o celular sobre a peça, faça com que a ela vibre e inicie a captura da vibração</p>";
        descricaoDemonstracao.setText(Html.fromHtml(formattedText));

        realizarEnsaio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent it = new Intent(DemonstracaoActivity.this, MainHisamoto.class);
                startActivity(it);
            }
        });
    }
}
