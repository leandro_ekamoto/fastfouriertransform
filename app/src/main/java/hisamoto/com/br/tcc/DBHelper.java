package hisamoto.com.br.tcc;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by hisamoto on 04/06/17.
 */

public class DBHelper extends SQLiteOpenHelper {

    // Nome do banco de dados
    private static final String NOME_BANCO = "ME-Wood";

    // Versão atual do banco de dados
    private static final int VERSAO_BANCO = 6;

    public DBHelper(Context context) {

        super(context, NOME_BANCO, null, VERSAO_BANCO);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql_ensaio = "CREATE TABLE ensaio( " +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT " +
                ", titulo TEXT NOT NULL " +
                ", largura_seccao REAL " +
                ", altura_seccao REAL"+
                ", massa_viga REAL"+
                ", vao_peca REAL"+
                ", me REAL" +
                ", frequencia REAL);";
        db.execSQL(sql_ensaio);

        String sql_ensaio_pontos = "CREATE TABLE ensaio_pontos( " +
                "_id INTEGER PRIMARY KEY AUTOINCREMENT " +
                ",_id_ensaio INTEGER " +
                ",ponto REAL " +
                ",pontoX REAL " +
                ", FOREIGN KEY (_id_ensaio) REFERENCES ensaio(_id));";
        db.execSQL(sql_ensaio_pontos);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        String sql_ensaio_pontos = "DROP TABLE IF EXISTS ensaio_pontos";
        db.execSQL(sql_ensaio_pontos);

        String sql_ensaio = "DROP TABLE IF EXISTS ensaio";
        db.execSQL(sql_ensaio);



        onCreate(db);
    }
}
