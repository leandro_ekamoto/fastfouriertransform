package hisamoto.com.br.tcc;

/**
 * Created by hisamoto on 04/06/17.
 */

public class EnsaioPonto {

    private long _id;
    private long _id_ensaio;
    private double ponto;
    private double pontoX;

    public EnsaioPonto() {

    }

    public EnsaioPonto(int _id_ensaio, double ponto) {
        this._id_ensaio = _id_ensaio;
        this.ponto = ponto;
    }

    public EnsaioPonto(long _id, long _id_ensaio, double ponto, double pontox) {
        this._id = _id;
        this._id_ensaio = _id_ensaio;
        this.ponto = ponto;
        this.pontoX = pontox;
    }

    public long get_id_ensaio() {
        return _id_ensaio;
    }

    public void set_id_ensaio(long _id_ensaio) {
        this._id_ensaio = _id_ensaio;
    }

    public double getPonto() {
        return ponto;
    }
    public double getPontoX() {
        return pontoX;
    }

    public void setPonto(double ponto) {
        this.ponto = ponto;
    }
    public void setPontoX(double ponto) {
        this.pontoX = ponto;
    }

    @Override
    public String toString() {
        return  "\nX: " +pontoX+" \nY: " + ponto+"";
    }
}
