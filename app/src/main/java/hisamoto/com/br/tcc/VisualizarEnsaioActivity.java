package hisamoto.com.br.tcc;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.Viewport;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import static java.lang.String.*;

public class VisualizarEnsaioActivity extends AppCompatActivity {

    GraphView graphFFT;
    LineGraphSeries<DataPoint> serieFFT;
    private Ensaio ensaio;
    private Context contexto;
    private String DEBUG_SHINDI = "DEBUG_SHINDI";

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visualizar_ensaio);

        // Comentado por causa do problema da actionbar da listagem de ensaios
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent it = getIntent();
        String id_ensaio = it.getStringExtra("id_ensaio");

        EnsaioModel ensaioModel = new EnsaioModel(getApplicationContext());
        ensaio = ensaioModel.getEnsaio(Integer.parseInt(id_ensaio));

        TextView titulo_ensaio = (TextView) findViewById(R.id.titulo_ensaio);
        TextView altura_seccao = (TextView) findViewById(R.id.altura_seccao);
        TextView largura_seccao = (TextView) findViewById(R.id.largura_seccao);
        TextView massa_viga = (TextView) findViewById(R.id.massa_viga);
        TextView vao_peca = (TextView) findViewById(R.id.vao_peca);
        TextView me = (TextView) findViewById(R.id.me);
        TextView frequencia = (TextView) findViewById(R.id.frequencia);
        Button listagemPontosFFT = (Button) findViewById(R.id.ListarPontosFFT);
        TextView maior_y = (TextView)findViewById(R.id.maior_y);
        TextView calculo_frequencia = (TextView)findViewById(R.id.calculo_frequencia);

        contexto = getApplicationContext();

        listagemPontosFFT.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent i = new Intent();

                i.setClass(contexto, ListagemPontosFFTActivity.class);

                i.putExtra("id_ensaio", ensaio.get_id());

                startActivity(i);
            }
        });

        assert titulo_ensaio != null;
        titulo_ensaio.setText(ensaio.getTitulo());
        assert altura_seccao != null;
        altura_seccao.setText("Altura secção - h (cm): " + ensaio.getAltura_seccao());
        assert largura_seccao != null;
        largura_seccao.setText(format("Largura secção - b (cm): %s", ensaio.getLargura_seccao()));
        assert massa_viga != null;
        massa_viga.setText(format("Massa da Viga - m (Kg): %s", ensaio.getMassa_viga()));
        assert vao_peca != null;
        vao_peca.setText(format("Vão - l (m): %s", ensaio.getVao_peca()));

        assert frequencia != null;
        frequencia.setText(format("Frequência - f: %s", ensaio.getFrequencia()));

        Locale.setDefault(Locale.US);

        NumberFormat format = NumberFormat.getInstance();
        format.setMaximumFractionDigits(3);
        format.setMinimumFractionDigits(2);
        format.setMaximumIntegerDigits(2);
        format.setRoundingMode(RoundingMode.HALF_UP);

        Double moe = Double.valueOf(format.format(ensaio.getMe()));

        assert me != null;
        me.setText(format("MOE: %s", moe));

        graphFFT = (GraphView) findViewById(R.id.graphFFT2);

        assert graphFFT != null;
        Viewport vpFFT = graphFFT.getViewport();
        vpFFT.setXAxisBoundsManual(false);

        vpFFT.setScalableY(false);

        List<EnsaioPonto> lista_pontos = ensaioModel.getEnsaioIDPontos(Integer.parseInt(id_ensaio));

        int qtd_pontos = lista_pontos.size(); // Antes era assim
        //int qtd_pontos = 100;

        DataPoint[] datapoints = new DataPoint[qtd_pontos];

        int i = 0;

        //valormoe = Double.valueOf(format.format(terceiro));


        double maior = -1;
        EnsaioPonto ensaioMaior = new EnsaioPonto();


        for(i=1;i<lista_pontos.size();i++)
        {
            if(lista_pontos.get(i).getPonto()>maior)
            {
                ensaioMaior = lista_pontos.get(i);
                maior = lista_pontos.get(i).getPonto();
            }
        }


        i = 0;
        for (EnsaioPonto ponto : lista_pontos) {



            if(i != 0){

                //datapoints[i] = new DataPoint(ponto.getPontoX() * 100 / qtd_pontos, ponto.getPonto());
                datapoints[i] = new DataPoint(ponto.getPontoX(), ponto.getPonto());
                Log.i("DEBUGSHIT","PONTOXSALVO: "+ponto.getPontoX());
            }
            else datapoints[i] =new DataPoint(0, 0);
            i++;

            if(i == qtd_pontos)
                break;
        }

        Double mmm = ensaioMaior.getPonto();
        maior_y.setText("Maior Y: "+mmm + " X: "+ensaioMaior.getPontoX());
        calculo_frequencia.setText("PontoX*100/Qtd.Pontos: " + (ensaioMaior.getPontoX()* 100 / qtd_pontos));

        Log.i(DEBUG_SHINDI,"Quantidade de Pontos: "+qtd_pontos);
        graphFFT.removeAllSeries();
        serieFFT = new LineGraphSeries<>(datapoints);

        serieFFT.setColor(Color.BLACK);

        graphFFT.addSeries(serieFFT);
        graphFFT.setTitle("FFT");

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Id correspondente ao botão Up/Home da actionbar
            case android.R.id.home:
                Intent i = new Intent();
                i.setClass(getApplicationContext(), ListaEnsaioCustomizadoActivity.class);
                startActivity(i);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
