package hisamoto.com.br.tcc;

/**
 * Created by hisamoto on 04/06/17.
 */

public class Ensaio {

    private String titulo;
    private int _id;
    private double largura_seccao;
    private double altura_seccao;
    private double massa_viga;
    private double vao_peca;
    private double me;
    private double frequencia;

    public Ensaio() {

    }

    public Ensaio(String titulo, Integer id, double largura_seccao, double altura_seccao, double massa_viga, double vao_peca, double me, double frequencia) {

        this.titulo = titulo;
        this._id = id;
        this.largura_seccao = largura_seccao;
        this.altura_seccao = altura_seccao;
        this.massa_viga = massa_viga;
        this.vao_peca = vao_peca;
        this.me = me;
        this.frequencia = frequencia;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public double getLargura_seccao() {
        return largura_seccao;
    }

    public void setLargura_seccao(double largura_seccao) {
        this.largura_seccao = largura_seccao;
    }

    public double getAltura_seccao() {
        return altura_seccao;
    }

    public void setAltura_seccao(double altura_seccao) {
        this.altura_seccao = altura_seccao;
    }

    public double getMassa_viga() {
        return massa_viga;
    }

    public void setMassa_viga(double massa_viga) {
        this.massa_viga = massa_viga;
    }

    public double getVao_peca() {
        return vao_peca;
    }

    public void setVao_peca(double vao_peca) {
        this.vao_peca = vao_peca;
    }

    public double getMe() {
        return me;
    }
    public double getFrequencia() {
        return frequencia;
    }

    public void setMe(double me) {
        this.me = me;
    }
    public void setFrequencia(double frequencia) {
        this.frequencia = frequencia;
    }
}
