package hisamoto.com.br.tcc.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import hisamoto.com.br.tcc.ListViewCustomizadoItem;
import hisamoto.com.br.tcc.R;

/**
 * @author Leandro Shindi
 * @version 1.0 26/06/15.
 */
public class ListViewCustomizadoAdapter extends BaseAdapter {

    private Context contexto;

    // Lista de itens a serem apresentados na tela
    private List<ListViewCustomizadoItem> itens;

    public ListViewCustomizadoAdapter(Context contexto, List<ListViewCustomizadoItem> itens) {

        this.contexto = contexto;
        this.itens = itens;
    }

    /**
     * Quantidade de itens a serem aprensentados na ListView
     *
     */
    @Override
    public int getCount() {

        return itens.size();
    }

    /**
     *  Retorna um item da ListView através de sua posição atual
     *
     */
    @Override
    public Object getItem(int posicao) {

        return itens.get(posicao);
    }

    /***
     * ID de um item. Em nosso caso usaremos sua posição como ID
     *
     * */
    @Override
    public long getItemId(int position) {

        return position;
    }

    /**
     * Aqui é onde a mágica é feita: cada item da nossa lista de itens
     * será adaptado para ser apresentado em uma linha da ListView.
     * É aqui que montamos nossa View customizada
     *
     * */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Item a ser apresentado na posição atual da ListView
        ListViewCustomizadoItem item = itens.get(position);

        // Criando uma instância de View a partir de um arquivo de Layout
        LayoutInflater inflater = (LayoutInflater) contexto.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.activity_listview_customizado, parent, false);

        // ID Ensaio
        TextView id_ensaio = (TextView) view.findViewById(R.id.id_ensaio);
        id_ensaio.setText(""+item.getId_ensaio());

        // Título Ensaio
        TextView titulo = (TextView) view.findViewById(R.id.titulo_ensaio);
        titulo.setText(item.getTitulo());

        // Configuração do ensaio
        TextView configuracao = (TextView) view.findViewById(R.id.configuracao);
        configuracao.setText(item.getConfiguracao());

        // Módulo de Elasticidade
        TextView me = (TextView) view.findViewById(R.id.me);
        me.setText("MOE: " + item.getMe().toString());

        // Zebrando a ListView
        if(position % 2 == 0) {

            view.setBackgroundColor(Color.rgb(223,223,223));
        } else {

            view.setBackgroundColor(Color.WHITE);
        }

        return view;
    }
}