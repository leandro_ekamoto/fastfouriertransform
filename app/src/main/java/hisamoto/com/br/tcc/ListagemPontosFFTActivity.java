package hisamoto.com.br.tcc;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.List;

public class ListagemPontosFFTActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listagem_pontos_fft);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Listagem de Pontos FFT");
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent it = getIntent();
        int id_ensaio = it.getIntExtra("id_ensaio", 0);

        ListView listaDePontos = (ListView) findViewById(R.id.lista_pontos);

        EnsaioModel ensaioModel = new EnsaioModel(getApplicationContext());
        //Ensaio ensaio = ensaioModel.getEnsaio(Integer.parseInt(id_ensaio));

        List<EnsaioPonto> lista_pontos = ensaioModel.getEnsaioIDPontos(id_ensaio);





        ArrayAdapter<EnsaioPonto> adapter = new ArrayAdapter<EnsaioPonto>(this,
                R.layout.lista_pontos, lista_pontos);
        listaDePontos.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        onBackPressed();
        return true;
    }

}
