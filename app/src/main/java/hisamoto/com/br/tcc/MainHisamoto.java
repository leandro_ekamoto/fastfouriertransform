package hisamoto.com.br.tcc;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class MainHisamoto extends AppCompatActivity implements SensorEventListener {

    private String DEBUG_TESTE = "DEBUG_TESTE";
    private String DEBUG_SHINDI = "DEBUG_SHINDI";
    private float x;
    private float y;
    private float z;

    private SensorManager mSensorManager;
    private Sensor mAcelerometro;

    int startTime;

    private double[] vector_x;
    private double[] vector_y;

    private int contador = 0;
    private boolean gravarPontos = false;

    private int n = 512;
    private ComplexFFT complexFFT;
    private int contador_pontos = 0;
    private double max_point_y = -1.0;
    private double max_point_x = -1.0;
    private double largura = 0;
    private double altura = 0;
    private double massa_viga = 0.0;
    private double vao_peca = 0.0;

    private Button CapturarVibracao;
    public ProgressDialog myDialog;
    private EditText larguraSeccao;
    private EditText alturaSeccao;
    private EditText massaViga;
    private EditText vaoPeca;
    private EditText tituloEnsaio;
    private Context contexto;
    private Complex[] y_processado;
    private double valormoe;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        contexto = getApplicationContext();

        CapturarVibracao = (Button) findViewById(R.id.CapturarVibracao);
        larguraSeccao = (EditText)findViewById(R.id.largura_seccao);
        alturaSeccao = (EditText)findViewById(R.id.altura_seccao);
        massaViga = (EditText)findViewById(R.id.massa_viga);
        vaoPeca = (EditText)findViewById(R.id.vao_peca);
        tituloEnsaio = (EditText)findViewById(R.id.titulo_ensaio);

        CapturarVibracao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(validaFormulario()) {
                    if(!gravarPontos) {

                        max_point_y = -1.0;
                        max_point_x = -1.0;
                        contador_pontos = 0;

                        contador = 0;
                        vector_x = new double[10000];
                        vector_y = new double[10000];
                        gravarPontos = true;
                    }

                    myDialog = new ProgressDialog(v.getContext());
                    myDialog.setMessage("Capturando pontos...");
                    myDialog.setCancelable(false);

                    myDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Calcular ME", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            gravarPontos = false;
                        }
                    });
                    myDialog.show();
                }
            }
        });

        /************************************* Iniciando captura de variação X, Y, Z *********************************/
        startTime = 0;
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mSensorManager.registerListener(this, mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_FASTEST);

        mAcelerometro = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        /******************************************* Limpando vetores *************************************/
        vector_x = new double[10000];
        Complex[] x = new Complex[n];
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    /***********************************************************
     * Quando ocorre alguma alteração no sensor do celular
     * ele é notificado aqui
     ***********************************************************/
    @Override
    public void onSensorChanged(SensorEvent event) {

        x = event.values[0];
        y = event.values[1];
        z = event.values[2];

        updateGraph(startTime++, x, y, z);
    }

    /*Verifica se número é potencia de 2*/
    private boolean ePotenciaDeDois(int numero) {

        int i =0;
        int numero_multi = 1;
        for(; i < 20 ; i++) {

            if(numero_multi == numero){

                return true;
            }

            numero_multi *= 2;
        }

        return false;
    }

    void updateGraph(final long timestamp, final float x, final float y, final float z) {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {

                if (gravarPontos || (contador_pontos != 0 && !ePotenciaDeDois(contador_pontos))) {

                    // Conta quantidade de pontos que estão sendo coletados
                    contador_pontos++;

                    // Possível erro
                    vector_x[contador] = contador;
                    vector_y[contador] = z;
                    contador++;
                    Log.i("DEBUGSHIT","CONTADOR_PONTOS:"+contador_pontos);

                } else if (contador_pontos > 0) {

                    int l = 0;
                    Complex[] vecy = new Complex[contador_pontos];

                    for (; l < contador_pontos; l++) {

                        vecy[l] = new Complex(vector_y[l], 0);

                    }

                    complexFFT = new ComplexFFT();
                    y_processado = null;
                    y_processado = complexFFT.fft(vecy);

                    for (int i = 0; i < contador_pontos; i++) {

                        double teste_y = y_processado[i].abs();

                        if (i == 0) {

                            teste_y = 0;
                        }

                        // Pegando pico máximo
                        if (teste_y > max_point_y) {

                            max_point_y = teste_y;
                            max_point_x = (vector_x[i]*100)/contador_pontos;
                            Log.i("DEBUGSHIT","Máximo X: [" + vector_x[i] + "] - " + max_point_x);
                        }
                    }

                    Log.i("DEBUGSHIT","Ponto máximo Y: " + max_point_y);
                    Log.i("DEBUGSHIT","Ponto máximo X: " + max_point_x);

                    altura = Float.valueOf(alturaSeccao.getText().toString());
                    largura = Float.valueOf(larguraSeccao.getText().toString());
                    massa_viga = Float.valueOf(massaViga.getText().toString());
                    vao_peca = Float.valueOf(vaoPeca.getText().toString());

                    double primeiro = (vao_peca * vao_peca * vao_peca);
                    double segundo = (largura * (altura * altura * altura))/12;
                    double terceiro = (max_point_x*max_point_x)*massa_viga*primeiro/(2.46*(segundo)*9.8);

                    // I = b.h³/12
                    Log.i(DEBUG_SHINDI, "max_point_y: " + max_point_y + "\nmassa_viga:" + massa_viga + "\nvao_peca:" + vao_peca + "\nlargura:" + largura + "\naltura" + altura);
                    //Log.i("DEBUGSHIT","max_point_y: " + max_point_y + "\nmassa_viga:" + massa_viga + "\nvao_peca:" + vao_peca + "\nlargura:" + largura + "\naltura" + altura);
                    //valormoe = ((max_point_y * max_point_y) * massa_viga * (vao_peca * vao_peca * vao_peca)) / (2.46 * (largura * (altura * altura * altura) / 12.0) * 9.8);

                    // Hisamoto
                    Log.i(DEBUG_TESTE, "ME: " + terceiro);

                    Log.i(DEBUG_SHINDI,"Primeiro: "+primeiro);
                    Log.i(DEBUG_SHINDI,"Segundo: "+segundo);
                    Log.i(DEBUG_SHINDI,"Frequencia: "+max_point_x);
                    Log.i(DEBUG_SHINDI,"ME: "+terceiro);

                    gravarPontos = false;
                    contador_pontos = 0;

                    vector_y = new double[10000];

                    Locale.setDefault(Locale.US);

                    NumberFormat format = NumberFormat.getInstance();
                    format.setMaximumFractionDigits(3);
                    format.setMinimumFractionDigits(2);
                    format.setMaximumIntegerDigits(2);
                    format.setRoundingMode(RoundingMode.HALF_UP);

                    valormoe = terceiro;

                    new AlertDialog.Builder(MainHisamoto.this)
                            .setTitle("Módulo de Elasticidade")
                            .setMessage("ME: " + valormoe+"\n\nDeseja salvar ensaio?")
                            .setCancelable(false)
                            .setNegativeButton("Cancelar",new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    //
                                }
                            })
                            .setPositiveButton("Salvar", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    EnsaioModel ensaioModel = new EnsaioModel(contexto);

                                    Ensaio ensaio = new Ensaio();
                                    ensaio.setTitulo(tituloEnsaio.getText().toString());
                                    ensaio.setLargura_seccao(Double.parseDouble(larguraSeccao.getText().toString()));
                                    ensaio.setAltura_seccao(Double.parseDouble(alturaSeccao.getText().toString()));
                                    ensaio.setMassa_viga(Double.parseDouble(massaViga.getText().toString()));
                                    ensaio.setVao_peca(Double.parseDouble(vaoPeca.getText().toString()));
                                    ensaio.setMe(valormoe);
                                    ensaio.setFrequencia(max_point_x);

                                    ContentValues values = new ContentValues();
                                    values.put("titulo", ensaio.getTitulo());
                                    values.put("largura_seccao", ensaio.getLargura_seccao());
                                    values.put("altura_seccao", ensaio.getAltura_seccao());
                                    values.put("massa_viga", ensaio.getMassa_viga());
                                    values.put("vao_peca", ensaio.getVao_peca());
                                    values.put("me", ensaio.getMe());
                                    values.put("frequencia", ensaio.getFrequencia());

                                    long ret = ensaioModel.inserirEnsaio(values);

                                    List<Ensaio> lista_ensaios = ensaioModel.getEnsaios();

                                    // Inseriu com sucesso
                                    if(ret > 0) {

                                        EnsaioPonto ensaioPonto = new EnsaioPonto();
                                        ContentValues values_ensaio_ponto = new ContentValues();

                                        int qtd_pont = y_processado.length;
                                        for (int i = 0; i < qtd_pont; i++) {

                                            ensaioPonto.set_id_ensaio(ret);
                                            ensaioPonto.setPonto(y_processado[i].abs());
                                            ensaioPonto.setPontoX(vector_x[i]);

                                            values_ensaio_ponto.put("_id_ensaio", ensaioPonto.get_id_ensaio());
                                            values_ensaio_ponto.put("ponto", ensaioPonto.getPonto());
                                            values_ensaio_ponto.put("pontoX", ensaioPonto.getPontoX());

                                            ensaioModel.inserirEnsaioPontos(values_ensaio_ponto);
                                        }

                                        vector_x = new double[10000];

                                        if(qtd_pont > 0) {

                                            List<EnsaioPonto> list = new ArrayList<EnsaioPonto>();
                                            list = ensaioModel.getEnsaioPontos();

                                            for (int i = 0; i < list.size(); i++) {

                                                Log.i("AnaliseEnsaioPonto:",list.get(i).get_id_ensaio() + " - " + list.get(i).getPonto());
                                            }
                                        }

                                    }
                                }
                            }).show();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.lista_ensaios) {

            Intent i = new Intent();
            i.setClass(getApplicationContext(), ListaEnsaioCustomizadoActivity.class);
            startActivity(i);

            return true;
        }

        if (id == R.id.sobre) {

            Intent i = new Intent();
            i.setClass(getApplicationContext(), SobreActivity.class);
            startActivity(i);

            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "MainHisamoto Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://hisamoto.com.br.tcc/http/host/path")
        );
        AppIndex.AppIndexApi.start(client, viewAction);
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        Action viewAction = Action.newAction(
                Action.TYPE_VIEW, // TODO: choose an action type.
                "MainHisamoto Page", // TODO: Define a title for the content shown.
                // TODO: If you have web page content that matches this app activity's content,
                // make sure this auto-generated web page URL is correct.
                // Otherwise, set the URL to null.
                Uri.parse("http://host/path"),
                // TODO: Make sure this auto-generated app deep link URI is correct.
                Uri.parse("android-app://hisamoto.com.br.tcc/http/host/path")
        );
        AppIndex.AppIndexApi.end(client, viewAction);
        client.disconnect();
    }

    /***
     * Valida parâmetros para cálculo de ME
     *
     * **/
    private boolean validaFormulario() {

        boolean invalido = true;

        if(!Validator.validateNotNull(vaoPeca, "Preencha o campo vão")) {

            invalido = false;
        }
        if(!Validator.validateNotNull(vaoPeca, "Preencha o campo vão")) {

            invalido = false;
        }

        if(!Validator.validateNotNull(massaViga, "Preencha o campo massa")) {

            invalido = false;
        }

        if(!Validator.validateNotNull(alturaSeccao, "Preencha o campo altura")) {

            invalido = false;
        }

        if(!Validator.validateNotNull(larguraSeccao, "Preencha o campo largura")) {

            invalido = false;
        }

        if(!Validator.validateNotNull(tituloEnsaio, "Preencha o campo título")) {

            invalido = false;
        }

        return invalido;
    }
}
