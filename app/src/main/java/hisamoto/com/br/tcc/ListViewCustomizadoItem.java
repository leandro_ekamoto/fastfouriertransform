package hisamoto.com.br.tcc;

/**
 * Classe que vai conter dos dados que via ser mostrado
 * em cada linha da ListView
 *
 * @author Leandro Shindi
 * @version 1.0 26/06/15.
 */
public class ListViewCustomizadoItem {

    private int id_ensaio;
    private String titulo;
    private String configuracao;
    private String status;
    private Double me;

    public ListViewCustomizadoItem(int id_ensaio, String titulo, String configuracao, Double me) {

        this.id_ensaio = id_ensaio;
        this.titulo = titulo;
        this.configuracao = configuracao;
        this.me = me;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getConfiguracao() {
        return configuracao;
    }

    public void setConfiguracao(String configuracao) {
        this.configuracao = configuracao;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Double getMe() {
        return me;
    }

    public void setMe(Double me) {
        this.me = me;
    }

    public int getId_ensaio() {
        return id_ensaio;
    }

    public void setId_ensaio(int id_ensaio) {
        this.id_ensaio = id_ensaio;
    }
}
