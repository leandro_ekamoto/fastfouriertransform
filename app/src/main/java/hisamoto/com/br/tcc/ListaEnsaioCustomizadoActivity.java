package hisamoto.com.br.tcc;

import android.app.ListActivity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.math.RoundingMode;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import hisamoto.com.br.tcc.adapter.ListViewCustomizadoAdapter;

public class ListaEnsaioCustomizadoActivity extends ListActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        List<ListViewCustomizadoItem> itens = new ArrayList<>();

        EnsaioModel ensaioModel = new EnsaioModel(getApplicationContext());

        List<Ensaio> lista_ensaios = ensaioModel.getEnsaios();


        Locale.setDefault(Locale.US); // &lt;==================================

        NumberFormat format = NumberFormat.getInstance();
        format.setMaximumFractionDigits(3);
        format.setMinimumFractionDigits(2);
        format.setMaximumIntegerDigits(2);
        format.setRoundingMode(RoundingMode.HALF_UP);

        double me_;
        for (int i = 0; i < lista_ensaios.size(); i++) {
            me_ = Double.valueOf(format.format(lista_ensaios.get(i).getMe()));
            //me_ =  Double.valueOf(String.format(Locale.ENGLISH, "%.2f", Math.floor(lista_ensaios.get(i).getMe())));

            itens.add(new ListViewCustomizadoItem(lista_ensaios.get(i).get_id(), lista_ensaios.get(i).getTitulo(), "h: "+lista_ensaios.get(i).getAltura_seccao()+
                    " b: " +lista_ensaios.get(i).getLargura_seccao()+ " m: "+lista_ensaios.get(i).getMassa_viga()+ " l: "+lista_ensaios.get(i).getVao_peca(), me_));
        }


        setListAdapter(new ListViewCustomizadoAdapter(getApplicationContext(), itens));
    }

    /***
     * Quando um item da lista é clicado ele retorna nesse método
     *
     */
    @Override
    protected void onListItemClick(ListView I, View v, int position, long id) {

        Intent i = new Intent();

        i.setClass(getApplicationContext(), VisualizarEnsaioActivity.class);

        TextView id_ensaio = (TextView)v.findViewById(R.id.id_ensaio);

        i.putExtra("id_ensaio", id_ensaio.getText());

        startActivity(i);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home) {
            Log.i("agenda", "botao voltar");
            finish();
        }

        // TODO Auto-generated method stub
        return super.onOptionsItemSelected(item);
    }
}
