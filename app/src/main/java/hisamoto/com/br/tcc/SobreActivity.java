package hisamoto.com.br.tcc;

import android.content.Intent;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

public class SobreActivity extends AppCompatActivity {

    private TextView decricaoSobre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sobre);

        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        decricaoSobre = (TextView) findViewById(R.id.descricao_sobre);

        String formattedText = "<br><br>Este aplicativo foi desenvolvido como parte do trabalho de conclusão do curso de Sistemas de Informação da Faculdade de Computação / Facom da Universidade Federal de Mato Grosso do Sul pelo acadêmico Leandro Shindi Ekamoto no ano 2017 sob orientação dos professores Luciana Montera (Facom) e Andrés Batista Cheung (Faculdade de Engenharias e Geografia)<br>" +
                "<br>Contatos:<br>" +
                "ekamoto.leandro@gmail.com<br>" +
                "montera@facom.ufms.br<br>" +
                "andrescheung@gmail.com<br>";
        decricaoSobre.setText(Html.fromHtml(formattedText));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Id correspondente ao botão Up/Home da actionbar
            case android.R.id.home:
                Intent i = new Intent();
                i.setClass(getApplicationContext(), MainHisamoto.class);
                startActivity(i);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
